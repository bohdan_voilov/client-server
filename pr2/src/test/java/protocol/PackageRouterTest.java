package protocol;

import entities.Store;
import network.FakePacketSender;
import network.FakeReceiver;
import network.PacketRouter;
import org.junit.jupiter.api.Test;
import services.AddItemPriceRequest;
import services.ChangeItemCountRequest;
import services.GetItemCountRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PackageRouterTest {
    @Test
    public void eachHandlerInSeparateThread(){
        FakeReceiver receiver = new FakeReceiver();
        PacketRouter router = new PacketRouter(receiver.protocolPacketStream(), new FakePacketSender(new ProtocolPacketWriter()));
        Store store = new Store();
        router.handleCommand(1, (GetItemCountRequest r) -> {
            synchronized (store) {
                int res = store.itemCount(r.getGroupName(), r.getItemName());
                return res;
            }
        });

        router.handleCommand(2, (ChangeItemCountRequest r) -> {
            synchronized (store) {
                store.addItemCount(r.getGroupName(), r.getItemName(), r.getItemCount());
            }
            return "Ok";
        });

        router.handleCommand(3, (ChangeItemCountRequest r) -> {
            synchronized (store) {
                store.addItemCount(r.getGroupName(), r.getItemName(), r.getItemCount());
            }
            return "Ok";
        });

        router.handleCommand(4, (String s) -> {
            synchronized (store) {
                store.addGroup(s);
            }
            return "Ok";
        });

        router.handleCommand(5, (AddItemPriceRequest r) -> {
            synchronized (store) {
                store.addItem(r.getGroupName(), r.getItemName(), r.getPrice());
            }
            return "Ok";
        });

        router.handleCommand(6, (AddItemPriceRequest r) -> {
            synchronized (store) {
                store.setItemPrice(r.getGroupName(), r.getItemName(), r.getPrice());
            }
            return "Ok";
        });
        router.run().blockingSubscribe(e -> {
            if (e.isPresent()) {
                e.get().printStackTrace();
            } else {
                System.out.println("No exception");
            }
        });
        assertEquals(8,Thread.activeCount());
    }
}
