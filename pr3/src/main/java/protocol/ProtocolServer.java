package protocol;

import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@AllArgsConstructor
public abstract class ProtocolServer {
    private final Map<Byte,Long> clientToCurrentRequestNum = new ConcurrentHashMap<>();
    protected boolean alreadyProcessed(byte clientId, long reqId){

        var lastProcessed = clientToCurrentRequestNum.getOrDefault(clientId,0l);
        if(clientId==2l){
            System.out.println("APL "+reqId+" "+lastProcessed);
        }
        if(reqId==0l){
            clientToCurrentRequestNum.put(clientId,0l);
            return false;
        }
        if(lastProcessed >= reqId || lastProcessed == 0l){
            return true;
        }
        clientToCurrentRequestNum.put(clientId,reqId);
        return false;
    }
}
