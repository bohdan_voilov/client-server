package protocol;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor @Getter
public class ProtocolMessage {
    private int commandCode;
    private int userId;
    private byte[] data;

}
