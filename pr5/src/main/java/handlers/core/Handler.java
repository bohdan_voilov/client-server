package handlers.core;

import com.sun.net.httpserver.HttpExchange;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
public class Handler {
    private String pathPattern;
    private String method;
    private ParameterizedHttpHandler callback;

    @SneakyThrows
    public boolean isMatching(HttpExchange exchange) {
        Pattern r = Pattern.compile(pathPattern);
        System.out.println(pathPattern);
        Matcher m = r.matcher(exchange.getRequestURI().getPath());
        if (m.matches() && method.equals(exchange.getRequestMethod())) {
            if (m.groupCount() == 1) {
                callback.addPathParam(m.group(1));
            }
            return true;

        }
        return false;
    }

    @SneakyThrows
    public void handle(HttpExchange exchange) {
        callback.handle(exchange);
    }
}
