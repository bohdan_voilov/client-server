package protocol;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.InputMismatchException;

public class ProtocolPacketReader {
    private ByteBuffer buffer;
    public ProtocolPacketReader(byte[] buffer){
        this.buffer = ByteBuffer.wrap(buffer);
    }
    public ProtocolPacket readPacket() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] head = new byte[14];
        buffer.get(head,0,14);
        buffer.position(0);
        readMagicByte();
        byte clientId = readClientId();
        long packetId = readPacketId();
        int messageLength = readMessageLength();
        byte[] headCRC16 = readCRC16();
        if(!validateCRC16(head,headCRC16)){
            throw new InputMismatchException("Head crc does not match");
        }
        byte[] message = readMessage(messageLength);
        byte[] messageCRC16 = readCRC16();
        if(!validateCRC16(message,messageCRC16)){
            throw new InputMismatchException("Message crc does not match");
        }

        Cipher cipher = Cipher.getInstance(ProtocolPacket.ENCRYPTION_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE,ProtocolPacket.ENCRYPTION_KEY);
        return new ProtocolPacket(clientId,packetId,new ProtocolMessageReader(cipher.doFinal(message)).readMessage());
    }

    private boolean validateCRC16(byte[] buffer, byte[] expectedHash){
        byte[] realHash = ProtocolPacket.crc16(buffer);
        return realHash[0] == expectedHash[0] && realHash[1] == expectedHash[1];
    }

    private void readMagicByte(){
        byte result = buffer.get();
        if(result != ProtocolPacket.MAGIC_BYTE){
            throw new InputMismatchException("Magic byte not found");
        }
    }

    private byte readClientId(){
        return buffer.get();
    }

    private long readPacketId(){
        return buffer.getLong();
    }

    private int readMessageLength(){
        return buffer.getInt();
    }

    private byte[] readCRC16(){
        return new byte[]{buffer.get(),buffer.get()};
    }

    private byte[] readMessage(int messageLength){
        byte[] res = new byte[messageLength];
        buffer.get(res);
        return res;
    }

}
