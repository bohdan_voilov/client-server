package handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import handlers.core.ParameterizedHttpHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import util.Token;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class LoginHandler extends ParameterizedHttpHandler {
    private Map<String,String> users = new HashMap<>(){{
        put("admin","098F6BCD4621D373CADE4E832627B4F6"); // password: test
    }};
    private final ObjectMapper objectMapper;
    public LoginHandler(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }
    @ToString
    @Getter
    @Setter
    private static class LoginRequest implements Serializable {
        String login;
        String password;
    }
    @AllArgsConstructor
    @Getter
    private static class LoginResponse implements Serializable {
        String token;
    }
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        LoginRequest req=null;
        try {
            req = objectMapper.readValue(exchange.getRequestBody(),LoginRequest.class);
        } catch (Exception e){
            e.printStackTrace();
            exchange.sendResponseHeaders(400,0);
            exchange.getResponseBody().close();
            return;
        }
        if(!users.get(req.getLogin()).equals(req.getPassword())){
            exchange.sendResponseHeaders(401,0);
            exchange.getResponseBody().close();
            return;
        }
        LoginResponse res = new LoginResponse(Token.token(req.getLogin()));
        var resBytes = objectMapper.writeValueAsBytes(res);
        System.out.println(new String(resBytes));
        System.out.println(resBytes.length);
        exchange.sendResponseHeaders(201,resBytes.length);
        var os = exchange.getResponseBody();
        os.write(resBytes);
        os.close();
    }
}