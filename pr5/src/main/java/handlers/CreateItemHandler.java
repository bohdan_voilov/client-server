package handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import entity.Item;
import entity.Store;
import handlers.core.ParameterizedHttpHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.io.Serializable;

@AllArgsConstructor
public class CreateItemHandler extends ParameterizedHttpHandler {
    private final Store store;
    private final ObjectMapper objectMapper;

    @AllArgsConstructor @Setter @Getter
    private static class Response implements Serializable {
        private int id;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            Item item = objectMapper.readValue(exchange.getRequestBody(), Item.class);
            int id = store.addItem(item);
            var res = objectMapper.writeValueAsBytes(new Response(id));
            System.out.println("CreateItemHandler "+new String(res));
            exchange.sendResponseHeaders(201,res.length);
            exchange.getResponseBody().write(res);
            exchange.getResponseBody().close();
        }catch (Exception e){
            e.printStackTrace();
            exchange.sendResponseHeaders(400,0);
            exchange.getResponseBody().close();
        }

    }
}
