package tcp;

import io.reactivex.rxjava3.core.Observable;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import network.PacketReceiver;
import protocol.ProtocolPacket;
import protocol.ProtocolPacketReader;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

@AllArgsConstructor
public class TCPPacketReceiver implements PacketReceiver {
    private final InputStream inputStream;

    @SneakyThrows
    @Override
    public ProtocolPacket protocolPacket() {
        var start = new byte[14];
        inputStream.read(start);
        var buffer = ByteBuffer.wrap(start);
        var length = buffer.getInt(10);
        var rest = new byte[2+length+2];
        inputStream.read(rest);
        byte[] packetBytes = ByteBuffer.allocate(start.length+rest.length).put(start).put(rest).array();
        return new ProtocolPacketReader(packetBytes).readPacket();
    }
}
