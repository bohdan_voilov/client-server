package network;

public interface PacketSender {
    void send(Handled handled);
}
