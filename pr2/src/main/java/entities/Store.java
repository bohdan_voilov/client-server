package entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Store {
    private final Map<String,Map<String,Item>> groups = new HashMap<>();
    public void addGroup(String name){
        this.groups.put(name,new HashMap<>());
    }

    public int itemCount(String groupName,String itemName){
        return this.groups.get(groupName).get(itemName).getCount();
    }

    public int addItemCount(String groupName,String itemName,int count){
        Item item = this.groups.get(groupName).get(itemName);
        int newCount = item.getCount()+count;
        item.setCount(item.getCount()+count);
        return newCount;
    }

    public void addItem(String groupName,String itemName,int price){
        this.groups.get(groupName).put(itemName,new Item(0,price));
    }

    public void setItemPrice(String groupName,String itemName,int price){
        Item item = groups.get(groupName).get(itemName);
        item.setPrice(price);
    }

}
