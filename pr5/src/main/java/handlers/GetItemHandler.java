package handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import entity.Store;
import handlers.core.ParameterizedHttpHandler;
import lombok.AllArgsConstructor;

import java.io.IOException;

@AllArgsConstructor
public class GetItemHandler extends ParameterizedHttpHandler {
    private final Store store;
    private final ObjectMapper objectMapper;
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            int id = Integer.valueOf(this.params.get(0));
            var item = store.getItem(id);
            if(item == null){
                exchange.sendResponseHeaders(404,0);
                exchange.getResponseBody().close();
                return;
            }
            var res = objectMapper.writeValueAsBytes(item);
            exchange.sendResponseHeaders(200,res.length);
            exchange.getResponseBody().write(res);
            exchange.getResponseBody().close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
