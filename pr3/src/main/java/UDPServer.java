import entities.Store;
import lombok.SneakyThrows;
import services.AddItemPriceRequest;
import services.ChangeItemCountRequest;
import services.GetItemCountRequest;
import tcp.StoreTCPServer;
import udp.StoreUDPServer;

public class UDPServer {
    @SneakyThrows
    public static void main(String[] args) {
        StoreUDPServer udpServer=new StoreUDPServer();
        Store store = new Store();
        udpServer.handleCommand(1, (GetItemCountRequest r) -> {
            synchronized (store) {
                int res = store.itemCount(r.getGroupName(), r.getItemName());
                return res;
            }
        });

        udpServer.handleCommand(2, (ChangeItemCountRequest r) -> {
            synchronized (store) {
                store.addItemCount(r.getGroupName(), r.getItemName(), r.getItemCount());
            }
            return "Ok";
        });

        udpServer.handleCommand(3, (ChangeItemCountRequest r) -> {
            synchronized (store) {
                store.addItemCount(r.getGroupName(), r.getItemName(), r.getItemCount());
            }
            return "Ok";
        });

        udpServer.handleCommand(4, (String s) -> {
            synchronized (store) {
                store.addGroup(s);
            }
            System.out.println("COMMAND 4 "+s);
            return "Ok";
        });

        udpServer.handleCommand(5, (AddItemPriceRequest r) -> {
            synchronized (store) {
                store.addItem(r.getGroupName(), r.getItemName(), r.getPrice());
            }
            return "Ok";
        });

        udpServer.handleCommand(6, (AddItemPriceRequest r) -> {
            synchronized (store) {
                store.setItemPrice(r.getGroupName(), r.getItemName(), r.getPrice());
            }
            return "Ok";
        });

        udpServer.run(8080);
    }
}
