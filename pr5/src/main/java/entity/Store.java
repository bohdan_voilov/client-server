package entity;

import java.util.HashMap;
import java.util.Map;

public class Store {
    Map<Integer,Item> items = new HashMap<>();

    public int addItem(Item item){
        int newId = items.keySet().stream().mapToInt(i->i).max().orElse(0)+1;
        items.put(newId,item);
        return newId;
    }

    public Item getItem(int id){
        return items.get(id);
    }

    public boolean updateItem(int id, Item item){
        if(!items.keySet().contains(id)){
            return false;
        }
        items.put(id,item);
        return true;
    }

    public boolean delete(int id){
        return items.remove(id) != null;
    }
}
