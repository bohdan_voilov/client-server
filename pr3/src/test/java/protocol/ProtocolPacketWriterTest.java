package protocol;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class ProtocolPacketWriterTest {
    private byte[] validPacketBytes = Base64.getDecoder().decode("EwAAAAAAAAAAAQAAACAuy+SQ0XBCSNQpKKsulhOdZj0Ot0J4kjbjHzLwhwemHj5Bym0=");
    private ProtocolPacket packet = new ProtocolPacket((byte)0,1l,new ProtocolMessage(1,1,"Hello World".getBytes()));
    @Test()
    public void writes() throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        byte[] bytes = new ProtocolPacketWriter().write(packet);
        Assertions.assertArrayEquals(validPacketBytes,bytes);
    }
}
