package udp;

import lombok.SneakyThrows;
import network.Serializer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class StoreUDPClient {
    private final byte id;
    private final int port;
    private long packetId = 0;

    private boolean serialBroken = false;

    public StoreUDPClient(byte id, int port) {
        this.id = id;
        this.port = port;
    }

    public StoreUDPClient(byte id, int port, boolean serialBroken) {
        this(id,port);
        this.serialBroken = serialBroken;
    }



    @SneakyThrows
    public String trySendMessage(String s){
        while (true){
            try {
                return sendMessage(s);
            } catch (Exception e){
                System.out.println("retrying in 1s");
                Thread.sleep(1000);
            }
        }
    }

    @SneakyThrows
    public String sendMessage(String s) {
        var socket = new DatagramSocket();
        socket.setSoTimeout(5000);
        var address = InetAddress.getByName("localhost");
        new UDPPacketSender(socket,address,port).send(id,serialBroken?1l:packetId,4,0,s);
        var buff = new byte[65535];
        var packet = new DatagramPacket(buff,buff.length);
        socket.receive(packet);
        packetId++;
        var protocolPacket = new UDPPacketReceiver(packet).protocolPacket();
        return (String)Serializer.deserialize(protocolPacket.getMessage().getData());
    }

}
