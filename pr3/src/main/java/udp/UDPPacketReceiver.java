package udp;


import network.PacketReceiver;
import protocol.ProtocolPacket;
import protocol.ProtocolPacketReader;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class UDPPacketReceiver implements PacketReceiver {
    private final ByteBuffer buff;
    public UDPPacketReceiver(DatagramPacket packet){
        this.buff = ByteBuffer.wrap(packet.getData());
    }

    @Override
    public ProtocolPacket protocolPacket() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        var start = new byte[14];
        buff.get(start,0,start.length);
        var buffer = ByteBuffer.wrap(start);
        var length = buffer.getInt(10);
        var rest = new byte[2+length+2];
        buff.get(rest,0, rest.length);
        byte[] packetBytes = ByteBuffer.allocate(start.length+rest.length).put(start).put(rest).array();
        return new ProtocolPacketReader(packetBytes).readPacket();
    }
}
