package network;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import protocol.ProtocolMessage;
import protocol.ProtocolPacket;
import protocol.ProtocolPacketWriter;

@AllArgsConstructor
public class FakePacketSender implements PacketSender {
    private ProtocolPacketWriter protocolPacketWriter;

    @SneakyThrows
    public void send(Handled handled){
        byte[] res = protocolPacketWriter.write(new ProtocolPacket(handled.getClientId(),handled.getPacketId(),new ProtocolMessage(0,0,Serializer.serialize(handled.getRes()))));
        System.out.println(Serializer.byteArrayToHex(res));
    }
}
