package protocol;

import lombok.AllArgsConstructor;

import java.nio.ByteBuffer;

public class ProtocolMessageReader {
    private ByteBuffer buffer;
    public ProtocolMessageReader(byte[] buffer){
        this.buffer = ByteBuffer.wrap(buffer);
    }

    public ProtocolMessage readMessage(){
        int commandCode = readCommandCode();
        int userId = readUserId();
        byte[] content = readContent();
        return new ProtocolMessage(commandCode,userId,content);
    }

    public int readCommandCode(){
        return this.buffer.getInt();
    }

    public int readUserId(){
        return this.buffer.getInt();
    }

    public byte[] readContent(){
        byte[] result = new byte[buffer.remaining()];
        buffer.get(result);
        return result;
    }
}
