CREATE TABLE IF NOT EXISTS items (
    id serial,
    title varchar(255),
    price int
);