import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String,Object> criterias = new HashMap();
        criterias.put("title", "Item 1");
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/store", "postgres", "secret")) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                var storeService = new StoreService(conn);
                //storeService.createItem("Item 1",200);
                var items = storeService.readItems();
                System.out.println(items);
                System.out.println(storeService.selectWith(criterias));
                items.get(0).setPrice(400);
                storeService.updateItem(items.get(0));
                System.out.println(storeService.selectWith(criterias));

            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
