import protocol.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Main {
    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }
    public static void main(String[] args) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        ProtocolPacket packet = new ProtocolPacket((byte)0,1l,new ProtocolMessage(1,1,"Hello World".getBytes()));
        byte[] packetBytes = new ProtocolPacketWriter().write(packet);
        ProtocolPacket readPacket = new ProtocolPacketReader(packetBytes).readPacket();
        System.out.println(new String(readPacket.getMessage().getData()));
        System.out.println(Base64.getEncoder().encodeToString(packetBytes));
    }
}
