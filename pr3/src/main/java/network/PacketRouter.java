package network;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import protocol.ProtocolPacket;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class PacketRouter {
    private final Observable<ProtocolPacket> packetStream;
    private final PacketSender packetSender;
    private final Map<Integer, Function> handlers = new HashMap<>();

    public PacketRouter(Observable<ProtocolPacket> packetStream, PacketSender packetSender) {
        this.packetStream = packetStream;
        this.packetSender = packetSender;
    }

    public <Req, Res> void handleCommand(int commandCode, Function<Req, Res> handler) {
        handlers.put(commandCode, handler);

    }

    public @NonNull Observable<Optional<Exception>> run() {

        return packetStream.toFlowable(BackpressureStrategy.MISSING)
                .parallel(4)
                .runOn(Schedulers.computation())
                .map(packet -> {
                    try {
                        System.out.println(Thread.currentThread().getName());
                        Object req = Serializer.deserialize(packet.getMessage().getData());
                        Object res = handlers.get(packet.getMessage().getCommandCode()).apply(req);

                        Handled h = new Handled(res, packet.getClientId(), packet.getPacketId());
                        packetSender.send(h);
                        Optional<Exception> otp = Optional.empty();
                        return otp;
                    } catch (Exception e) {
                        return Optional.of(e);
                    }
                }).sequential().toObservable();
    }
}
