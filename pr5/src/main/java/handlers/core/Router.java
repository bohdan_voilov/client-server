package handlers.core;

import lombok.SneakyThrows;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import com.sun.net.httpserver.Authenticator;


public class Router {
    private final List<Handler> handlers = new ArrayList<>();
    private final InetSocketAddress address;
    private final int backlog;
    public Router(InetSocketAddress address, int backlog){
        this.address=address;
        this.backlog=backlog;
    }

    public Router addRoute(Handler h){
        this.handlers.add(h);
        return this;
    }

    @SneakyThrows
    public void start(Authenticator auth){
        HttpServer server = HttpServer.create();

        server.bind(this.address,this.backlog);
        var ctx= server.createContext("/", exchange -> {
            handlers.stream().filter(handler -> handler.isMatching(exchange)).findFirst().ifPresentOrElse(handler -> handler.handle(exchange),()->{
                try {
                    exchange.sendResponseHeaders(404,0);
                    exchange.getResponseBody().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });
        ctx.setAuthenticator(auth);
        server.setExecutor(null);
        server.start();
    }
}
