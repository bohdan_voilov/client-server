package tcp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import network.PacketReceiver;
import network.Serializer;
import protocol.ProtocolPacket;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

@AllArgsConstructor
public class StoreTcpClient {
    private final String ip;
    private final int port;

    @SneakyThrows
    public String trySendMessage(String s){
        while (true){
            try{
                return sendMessage(s);
            } catch (Exception e) {
                System.out.println("reattempting in 1sec");
                Thread.sleep(1000);
            }
        }
    }

    @SneakyThrows
    public String sendMessage(String s) throws IOException {
        var socket = new Socket(ip,port);
        var inputStream = socket.getInputStream();
        var outputStream = socket.getOutputStream();
        var packetSender = new TCPPacketSender(outputStream);
        var packetReceiver = new TCPPacketReceiver(inputStream);
        var sent = false;

        packetSender.send((byte)0,0l,4,0,s);

        ProtocolPacket packet;
        try {
            packet = packetReceiver.protocolPacket();
        } catch (Exception e){
            e.printStackTrace();
            return "";
        }
        socket.close();
        return (String) Serializer.deserialize(packet.getMessage().getData());

    }
}
