package handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import entity.Item;
import entity.Store;
import handlers.core.ParameterizedHttpHandler;
import lombok.AllArgsConstructor;

import java.io.IOException;

@AllArgsConstructor
public class UpdateItemHandler extends ParameterizedHttpHandler {
    private final Store store;
    private final ObjectMapper objectMapper;
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        int id = Integer.valueOf(this.params.get(0));
        var item = objectMapper.readValue(exchange.getRequestBody(), Item.class);
        var exists = store.updateItem(id,item);
        if(exists){
            exchange.sendResponseHeaders(204,0);
            exchange.getResponseBody().close();
            return;
        }
        exchange.sendResponseHeaders(404,0);
        exchange.getResponseBody().close();
        return;
    }
}
