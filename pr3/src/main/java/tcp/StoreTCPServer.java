package tcp;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import network.Handled;
import network.PacketReceiver;
import network.PacketSender;
import network.Serializer;
import protocol.ProtocolPacket;
import protocol.ProtocolServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class StoreTCPServer extends ProtocolServer {
    private final Map<Integer, Function> handlers = new HashMap<>();

    @AllArgsConstructor
    private class ServerThread extends Thread {
        private final PacketReceiver packetReceiver;
        private final PacketSender packetSender;

        @SneakyThrows
        @Override
        public void run() {
            System.out.println("Server run");
            ProtocolPacket packet;
            try {
               packet = packetReceiver.protocolPacket();
            } catch (Exception e){
                e.printStackTrace();
                return;
            }
            var alreadyProcessed = alreadyProcessed(packet.getClientId(),packet.getPacketId());
            if(alreadyProcessed){
                packetSender.send(new Handled("Already processed",packet.getClientId(),packet.getPacketId()));
                return;
            }
            Object req = Serializer.deserialize(packet.getMessage().getData());
            Object res = handlers.get(packet.getMessage().getCommandCode()).apply(req);
            Handled h = new Handled(res, packet.getClientId(), packet.getPacketId());
            packetSender.send(h);

        }
    }
    public <Req, Res> void handleCommand(int commandCode, Function<Req,Res> handler){
        handlers.put(commandCode,handler);
    }

    @SneakyThrows
    public void run(int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port,1);
        System.out.println("TCP server started at "+port);
        while(true){
            try {
                Socket socket = serverSocket.accept();
                System.out.println("Accepted");
                new ServerThread(new TCPPacketReceiver(socket.getInputStream()),new TCPPacketSender(socket.getOutputStream())).start();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
